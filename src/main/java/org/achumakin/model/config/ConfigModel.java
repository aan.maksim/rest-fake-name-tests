package org.achumakin.model.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigModel {

    private EnvironmentModel acc;
    private EnvironmentModel local;
    private EnvironmentModel docker;
    private EnvironmentModel compose;

}
