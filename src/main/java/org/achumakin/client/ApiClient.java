package org.achumakin.client;

import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.achumakin.model.NameModel;
import org.achumakin.util.YamlReader;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;

@Slf4j
public class ApiClient {

    private final RequestSpecification reqSpec;

    public ApiClient() {
        RestAssured.filters(new RequestLoggingFilter(LogDetail.URI), new ResponseLoggingFilter(LogDetail.BODY));
        var baseUrl = getBaseUrl();
        reqSpec = given()
                .baseUri(baseUrl)
                .config(RestAssuredConfig.config().httpClient(
                        HttpClientConfig.httpClientConfig().setParam("http.connection.timeout", 5000)))
                .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON);
        log.info("Configured request specification with base url '{}'", baseUrl);
    }

    public NameModel getName() {
        return reqSpec.get("/name").as(NameModel.class);
    }

    private String getBaseUrl() {
        var yamlReader = new YamlReader();
        var config = Objects.requireNonNull(yamlReader.readConfig()).getEnv();
        var env = System.getProperty("ENV", "acc");
        var envConfig = switch (env) {
            case "acc" -> config.getAcc();
            case "local" -> config.getLocal();
            case "docker" -> config.getDocker();
            case "compose" -> config.getCompose();
            default -> throw new RuntimeException(String.format("base URL is not defined for '%s' environment", env));
        };
        return getBaseUrl(envConfig.getProtocol(), envConfig.getHost(), envConfig.getPort());
    }

    /*
    (http, localhost, 8080) -> http://localhost:8080
    (https, remote.host.ip, null) -> https://remote.host.ip
     */
    private String getBaseUrl(String protocol, String host, String port) {
        return Stream.of(String.format("%s://%s", protocol, System.getProperty("SERVICE_HOST", host)), port)
                     .filter(s -> s != null && !s.isEmpty())
                     .collect(Collectors.joining(":"));
    }

}
